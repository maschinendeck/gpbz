#pragma once

#include <DNSServer.h>
#include <ESP8266WiFi.h>
#include <sstream>
#include <string>
#include <vector>

namespace maschinendeck::util {
	class Logger;
}

namespace maschinendeck {
class GPBZ;

struct Network {
	std::string ssid;
	int32_t channel;
	int32_t rssi;
	bool open;

	explicit Network(int8_t id) {
		this->ssid    = WiFi.SSID(id).c_str();
		this->channel = WiFi.channel(id);
		this->rssi    = WiFi.RSSI(id);
		this->open    = WiFi.encryptionType(id) == ENC_TYPE_NONE;
	}

	operator std::string() const {
		std::stringstream buffer;
		buffer << "{";
		buffer << "\"ssid\" : \"" << this->ssid << "\", ";
		buffer << "\"channel\" : " << this->channel << ", ";
		buffer << "\"rssi\" : " << this->rssi << ", ";
		buffer << "\"open\" : " << (this->open ? "true" : "false");
		buffer << "}";

		return buffer.str();
	}
};

class Wireless {
	private:
		static const IPAddress   IP;
		static const IPAddress   Gateway;
		static const IPAddress   Netmask;
		static constexpr uint8_t DNSPort           = 53;
		static constexpr uint8_t ConnectionTimeout = 15;

		GPBZ& app;
		maschinendeck::util::Logger& log;
		DNSServer dnsServer;
	public:
		explicit Wireless(GPBZ& app);
		~Wireless() = default;

		Wireless(const Wireless&) = delete;
		Wireless(Wireless&&) = delete;

		Wireless& operator =(const Wireless&) = delete;
		Wireless& operator =(Wireless&&) = delete;

		[[nodiscard]] bool apMode();
		[[nodiscard]] bool clientMode(std::string ssid, std::string password, int32_t channel);	

		std::vector<Network> scan() const;

		void update();
};

}