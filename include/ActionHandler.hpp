#pragma once

#include <PubSubClient.h>

#include "util/Settings.hpp"

namespace maschinendeck::util {
	class Logger;
}

namespace maschinendeck {
class GPBZ;

class ActionHandler {
	private:
		constexpr static const char* ClientID   = "GPBZ";
		constexpr static uint8_t MQTTMaxRetries = 5;
		GPBZ& app;
		util::Logger& log;	
	public:
		ActionHandler(GPBZ& app);
		~ActionHandler() = default;

		ActionHandler(const ActionHandler&) = delete;
		ActionHandler(ActionHandler&&) = delete;

		ActionHandler& operator=(const ActionHandler&) = delete;
		ActionHandler& operator=(ActionHandler&&) = delete;

		[[nodiscard]] bool runMQTT(const IPAddress& server, uint16_t port, const std::string& user, const std::string& password, const std::string& topic, const std::string& payload) const;
		[[nodiscard]] bool runWifi(const std::string& url) const;
};

}