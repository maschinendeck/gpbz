#pragma once

#include <ESP8266WebServer.h>

namespace maschinendeck {
class GPBZ;

	namespace util {
		class Logger;
	}

class WebInterface {
	private:
		constexpr static uint8_t NoConnectionTimeout = 90;

		GPBZ& app;
		util::Logger& log;
		ESP8266WebServer socket;
		bool hadConnection;
		size_t timeout;
	public:
		explicit WebInterface(GPBZ& app);
		~WebInterface() = default;

		WebInterface(const WebInterface&) = delete;
		WebInterface(WebInterface&&) = delete;

		WebInterface& operator =(const WebInterface&) = delete;
		WebInterface& operator =(WebInterface&&) = delete;

		void init();
		void listen();
		void update();
};

}