#pragma once

#include "DoubleResetDetector.hpp"
#include "WebInterface.hpp"
#include "Wireless.hpp"
#include "util/Logger.hpp"
#include "util/Settings.hpp"
#include "StatusLight.hpp"

namespace maschinendeck {

using Seconds = int;
using RTCMemoryAddress = int;

class GPBZ {
	private:
		constexpr static Seconds          DRDTimeout = 3;
		constexpr static RTCMemoryAddress DRDAddress = 0;

		DoubleResetDetector drd;
		util::Logger logger_;
		util::Settings settings_;
		StatusLight statusLight_;
		Wireless wifi_;
		WebInterface webInterface;

		uint32_t lastTime;

		void regularOperation();
	public:
		GPBZ();
		~GPBZ() = default;

		GPBZ(const GPBZ&) = delete;
		GPBZ(GPBZ&&) = delete;

		GPBZ& operator =(const GPBZ&) = delete;
		GPBZ& operator =(GPBZ&&) = delete;

		void update();
		void sleep();
		void restart();

		inline const Wireless& wifi() const {
			return this->wifi_;
		}

		inline util::Logger& logger() {
			return this->logger_;
		}

		inline util::Settings& settings() {
			return this->settings_;
		}

		inline StatusLight& statusLight() {
			return this->statusLight_;
		}
};

}