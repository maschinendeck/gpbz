#pragma once

#include <Adafruit_NeoPixel.h>

namespace maschinendeck::util {
	class Logger;
}

namespace maschinendeck {
class GPBZ;

//using Color = CRGB;
struct Color {
	uint8_t r, g, b, w;

	operator uint32_t() {
    	return ((uint32_t)r << 16) | ((uint32_t)g << 8) | b;
	}
};

inline constexpr Color ColorFromRGB(uint8_t r, uint8_t g, uint8_t b, uint8_t w) {
	Color color = {r, g, b};
	return color;
}

class StatusLight {
	public:
	 	constexpr static uint8_t WS2812_PIN          = 3;
	 	constexpr static uint8_t BOOST_CONVERTER_PIN = 13;
	 	constexpr static uint8_t WS2812_COUNT        = 4;

		enum class Direction {
			UP,
			DOWN
		};
	private:
		util::Logger& log;
		Adafruit_NeoPixel pixels;
		bool pulse;
		float intensity;
		Direction direction;
		Color color;

		constexpr static float BrightnessMultiplier = 300.0f;
	public:
		constexpr static Color COLOR_ERROR   {255, 0, 0, 0};
		constexpr static Color COLOR_WARNING {255, 255, 2, 0};
		constexpr static Color COLOR_INFO    {27, 73, 255, 0};
		constexpr static Color COLOR_SUCCESS {0, 255, 0, 0};
		constexpr static Color COLOR_WHITE   {0, 0, 0, 255};

		explicit StatusLight(GPBZ& app);
		~StatusLight() = default;

		StatusLight(const StatusLight&) = delete;
		StatusLight(StatusLight&&) = delete;

		StatusLight& operator =(const StatusLight&) = delete;
		StatusLight& operator =(StatusLight&&) = delete;

		void set(Color color, bool pulse = false);
		void update(float deltaTime);
		void shutdown() const;
};

}