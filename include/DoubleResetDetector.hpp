#pragma once

#include <Arduino.h>

namespace maschinendeck {

class DoubleResetDetector {
	private:
		constexpr static uint32_t Set          = 0xD0D01234;
		constexpr static uint32_t Clear        = 0xD0D04321;

		constexpr static size_t TimeoutSeconds = 3;
		constexpr static size_t Address        = 0x00;

		bool detected_;
		bool currentlySetting;
		uint32_t flag;
		size_t timeout;

	public:
		DoubleResetDetector();
		~DoubleResetDetector() = default;

		DoubleResetDetector(const DoubleResetDetector&) = delete;
		DoubleResetDetector(DoubleResetDetector&&) = delete;

		DoubleResetDetector& operator =(const DoubleResetDetector&) = delete;
		DoubleResetDetector& operator =(DoubleResetDetector&&) = delete;

		bool detected();
		void clear();		

		void update();
};

}