#pragma once

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>

namespace maschinendeck::util {

class Logger {
	private:
		std::vector<std::string> lines;
		std::stringstream currentLine;
	public:
		constexpr static char endl = '\n';

		Logger() = default;
		~Logger() = default;

		operator std::string() const {
			std::stringstream buffer;

			for (auto& line : this->lines) {
				buffer << line << "\r\n";
			}

			return buffer.str();
		}

		template <typename T>
		Logger& operator <<(const T& text) {
			#ifdef DEBUG
			this->currentLine << text;
			#else
			(void) text;
			#endif
			return *this;
		}

		Logger& operator<<(std::ostream& (*ostream_function)(std::ostream&)) {
			#ifdef DEBUG
			this->currentLine.flush();
			Serial.println(currentLine.str().c_str());
			this->currentLine.str("");
			this->currentLine.clear();
			#endif

			return *this;
		}

};

template<>
Logger& Logger::operator <<(const IPAddress& text);

template<>
Logger& Logger::operator <<(uint8_t const& mac);

}