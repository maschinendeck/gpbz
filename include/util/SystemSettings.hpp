#pragma once

#include <string>
#include <ESP8266WiFi.h>

namespace maschinendeck {

enum class ActionMode {
	http,
	mqtt,
	none
};

constexpr static const char* ActionModeJSON[] = {
	"http",
	"mqtt",
	"none"
};

std::string ActionModeToJSON(ActionMode mode);
ActionMode ActionModeFromJSON(const std::string& json);


}

namespace maschinendeck::util {

struct SystemSettings {
	std::string wifiSsid     = "";
	std::string wifiPassword = "";
	std::string targetURL    = "";
	std::string mqttUser     = "";
	IPAddress mqttServer     = IPAddress();
	std::string mqttPassword = "";
	std::string mqttTopic    = "gpbz/action";
	std::string mqttMessage  = "PRESSED";
	uint16_t mqttPort        = 1883;
	ActionMode actionMode    = ActionMode::http;
	int32_t wifiChannel      = 0; //@readonly
	uint8_t apMAC[6]         = {0, 0, 0, 0, 0, 0}; //@ignore 
	IPAddress wifiIP         = IPAddress(); //@readonly
	IPAddress wifiGateway    = IPAddress(); //@readonly
	IPAddress wifiNetmask    = IPAddress(); //@readonly
	IPAddress wifiDNS        = IPAddress(); //@readonly

	const SystemSettings& operator->() const {
		return *this;
	}
};

}