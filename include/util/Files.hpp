#pragma once

#include <LittleFS.h>
#include <sstream>

namespace maschinendeck::util {

std::stringstream fileContents(fs::File& file) {
	std::stringstream buffer;
	while (file.available()) {
		char car = file.read();
		buffer << car;
	}

	return buffer;
}

}