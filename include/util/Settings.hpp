#pragma once

#include <Arduino.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

#include "util/SystemSettings.hpp"

namespace maschinendeck {
	class GPBZ;

	void convertFromJson(JsonVariantConst& src, ActionMode& dst);
	bool convertToJson(const ActionMode& src, JsonVariant dst);
	bool canConvertFromJson(JsonVariantConst src, const ActionMode& dst);

	template <class... T>
	constexpr bool always_false = false;

	template <typename T>
	T convert(const std::string& value) {
		if constexpr (std::is_integral_v<T>) {
			return static_cast<T>(std::stoi(value));
		} else {
			static_assert(
				always_false<T>,
				"The supplied type is not supported, missing template specialization in `Settings.hpp`. For more info see below."
			);
		}
	}

	template <>
	inline IPAddress convert(const std::string& value) {
		IPAddress ip;
		ip.fromString(value.c_str());
		return ip;
	}

	template <>
	inline ActionMode convert(const std::string& value) {
		return ActionModeFromJSON(value);
	}

	template <>
	inline std::string convert(const std::string& value) {
		return std::move(value);
	} 

}

void convertFromJson(JsonVariantConst& src, IPAddress& dst);
bool convertToJson(const IPAddress& src, JsonVariant dst);
bool canConvertFromJson(JsonVariantConst src, const IPAddress& dst);

namespace std {
	void convertFromJson(JsonVariantConst& src, std::vector<uint8_t>& dst);
	bool convertToJson(const std::vector<uint8_t>& src, JsonVariant dst);
	bool canConvertFromJson(JsonVariantConst src, const std::vector<uint8_t>& dst);
}

namespace maschinendeck::util {
	class Logger;
	class Settings {
		private:
			constexpr static size_t JSONSize = 1024;
			constexpr static const char* Filename = "/settings.json";

			maschinendeck::GPBZ& app;
			Logger& log;
			std::shared_ptr<SystemSettings> settings;
			StaticJsonDocument<Settings::JSONSize> jsonDoc;
			std::string json_;
			std::string mac_;

			void init();
			void toJSON();
		public:
			Settings(maschinendeck::GPBZ& app);

			void clear();
			void load();
			void save();
			void set(const std::string& key, const std::string& value);

			const std::string& json();

			inline const std::shared_ptr<SystemSettings>& operator->() const {
				return this->settings;
			}

			inline void forgetCachedWifiAttributes(bool chained = false) {
				this->settings->wifiIP.clear();
				this->settings->wifiGateway.clear();
				this->settings->wifiNetmask.clear();
				this->settings->wifiDNS.clear();
				this->settings->wifiChannel = 0;
				memset(this->settings->apMAC, 0x00, std::size(this->settings->apMAC));

				if (!chained)
					this->save();
			}

			inline void forgetWifi() {
				this->forgetCachedWifiAttributes(true);
				this->settings->wifiSsid.clear();
				this->settings->wifiPassword.clear();

				this->save();
			}

			inline bool wifiSettingsPresent() const {
				return this->settings->wifiSsid.length() > 0 &&
					this->settings->wifiPassword.length() > 0;
			}

			const std::string& mac() {
				if (this->mac_.size() > 0)
					return this->mac_;

				std::stringstream buffer;

				for (uint8_t i = 0; i < 6; ++i) {
					buffer << std::hex << std::setw(2) << static_cast<int>(this->settings->apMAC[i]);
					if (i < 5)
						buffer << ':';
				}
				this->mac_ = buffer.str();

				return this->mac_;
			}
	};
}

