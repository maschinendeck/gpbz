# GPBZ
– an ESP8266 firmware for Buzzers that do stuff.

![GPBZ web interface](./assets/web_interface.png)

If not configured, this firmware will start an access point with the `SSID` of
*"GPBZ"*, where a web interface is provided to configure your setup.

If configured, the ESP will connect to the specified network as fast as possible
after reset and perform the configured action. Currently supported actions are:

* Performing a `GET` request on an specified URL using either `http` or `https`
* Sending a `MQTT` payload to speciefied MQTT-server on specified topic

After performing the action, the ESP will go into deepsleep again.

**If no connection to the web interface is happening within 90 seconds, the ESP
will also go into deepsleep.**

## Building
This project is built using [`platform.io`][platformio]. It provides three build
tasks:

* `release`
* `debug`
* `debug-verbose`

For production use, select the `release` target. This will stop all Serial
communication from happening, so the buzzer can run as fast as possible.

1. Run the `Platform -> Upload Filesystem Image` task
2. Run the `General -> Upload` task

After this, you should see a wifi network called *"GPBZ"*. Connect to this
within 90 seconds and configure your buzzer from there.

If you need to change your configuration, just double press the buzzer, so it
opens its network in access point mode again *(can be a bit flaky, as timings
are very tight)*.

The `debug` build task enables debug output via serial, the `debug-verbose` task
adds additional debugging from the `ESP8266WiFi` and `ESP8266WebServer`
libraries.


[platformio]: https://platformio.org