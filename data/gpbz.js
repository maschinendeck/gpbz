const detectedLanguage = "language" in navigator ? navigator.language.substring(0, 2) : "de"
const savedLanguage    = localStorage.getItem("lang");

let Lang        = savedLanguage ? savedLanguage : (detectedLanguage === "de" ? "de" : "en");
let I18N        = null;
let Form        = null;
let NetworkList = null;
let Settings    = {
	actionMode : "http"
};

const StaticRequest = Object.freeze({
	CLEAR_CACHE : 1,
	CLEAR_ALL   : 2,
	RESET       : 3
});

const $ = selector => {
	const nodes = document.querySelectorAll(selector);

	return nodes.length === 1 ? nodes[0] : nodes;
};

const fromTemplate = (id, data = {}) => {
	const template    = $(`#${id}`).content.firstElementChild;
	const htmlElement = template.cloneNode(true);

	for (const [key, value] of Object.entries(data)) {
	 	const regex = new RegExp(`{{${key}}}`, "mgi");
		htmlElement.innerHTML = htmlElement.innerHTML.replace(regex, value);
	}

	return htmlElement;
};

const refreshInputs = () => {
	if (!Settings)
		return;
	for (const [key, value] of Object.entries(Settings)) {
		const element = $(`input[name=${key}]`);
		if (!element)
			continue;
		element.value = value;
	}
	switch(Settings.actionMode) {
		case "http":
			$("li[data-toggle=http]").classList.add("active");
			$("#wifi").classList.add("active");
			break;
		case "mqtt":
			$("li[data-toggle=mqtt]").classList.add("active");
			$("#mqtt").classList.add("active");
			break;
	}
};

const setSSID = (event, name) => {
	event.preventDefault();

	$("input[name=wifiSsid]").value    = name;
	$("input[name=wifiPassword").value = "";
};

const signalIcon = strength => {
	let icon = "full";

	if (strength <= -67)
		icon = "75";
	else if (strength <= -70)
		icon = "50";
	else if (strength <= -80)
		icon = "low";

	return `<span class="icon icon-wifi_${icon}"></span>`;
};

const scan = event => {
	if (event)
		event.preventDefault();

	NetworkList.innerHTML = "";
	NetworkList.append(fromTemplate("spinner"));
	fetch("/scan").then(response => response.json()).then(json => {
		NetworkList.innerHTML = "";
		for (const network of json) {
			const element = fromTemplate("networkItem", {
				...network,
				open     : network.open ? "" : "<span class='icon icon-lock'></span>",
				strength : signalIcon(network.rssi)
			});
			element.addEventListener("click", event => setSSID(event, network.ssid));
			NetworkList.append(element);
		}
	}).catch(error => console.error(error));
};

const save = event => {
	event.preventDefault();

	const data = new FormData(Form);
	for (const key of Object.keys(Settings)) {
		const input = $(`input[name=${key}]`);
		if (input.hasOwnProperty("value"))
			Settings[key] = input.value;
		data.append(key, Settings[key]);
	}

	fetch("/settings", {
		method : "POST",
		body   : new URLSearchParams(data)
	}).then(() => {
		const btn = $("#saveBtn");
		btn.classList.add("saved");
		setTimeout(() => {
			btn.classList.remove("saved");
		}, 2000)
	}).catch(error => console.error(error));
}

const staticRequest = (event, request) => {
	event.preventDefault();

	let question = "";
	let target   = "";

	switch(request) {
		case StaticRequest.CLEAR_CACHE:
			question = "Willst du den WiFi-Cache wirklich löschen?";
			target   = "/clearCache";
			break;
		case StaticRequest.CLEAR_ALL:
			question = "Willst du wirklich alle Einstellungen zurücksetzen?";
			target   = "/resetSettings";
			break;
		case StaticRequest.RESET:
			question = "Willst du wirklich neustarten?";
			target   = "/restart";
			break;
	}
	if (I18N[Lang].hasOwnProperty(question))
		question = I18N[Lang][question];

	if (confirm(question)) {
		fetch(target).then(() => {
			window.self.location.reload();
		}).catch(error => console.error(error));
	}
};

const loadSettings = () => {
	fetch("/settings").then(response => response.json()).then(json => {
		Settings = json;
		refreshInputs();
	}).catch(error => console.error(error));
};

const localize = () => {
	if (I18N === null) {
		const langSwitcher = $("#langSwitcher");
		langSwitcher.addEventListener("change", switchLanguage);
		for (const option of langSwitcher.querySelectorAll("option")) {
			if (option.value === Lang)
				option.selected = true;
		}

		fetch("/i18n.json").then(response => response.json()).then(json => {
			I18N = json;
			localize();
		}).catch(error => console.error(error));
		return;
	}
	const units = $("[data-i18n]");

	for (const unit of units) {
		const identifier = unit.getAttribute("data-i18n");
		if (!I18N[Lang].hasOwnProperty(identifier)) {
			unit.innerHTML = identifier;
			continue;
		}
		unit.innerHTML = I18N[Lang][identifier];
	}
};

const switchLanguage = event => {
	Lang = event.target.value;
	localStorage.setItem("lang", event.target.value);
	localize();
}

document.addEventListener("DOMContentLoaded", () => {
	localize();

	Form        = $("#form");
	NetworkList = $("#networkList");

	Form.addEventListener("submit", save);
	$("#scan").addEventListener("click", scan);

	$("#rCache").addEventListener("click", event => staticRequest(event, StaticRequest.CLEAR_CACHE));
	$("#rAll").addEventListener("click", event => staticRequest(event, StaticRequest.CLEAR_ALL));
	$("#rRestart").addEventListener("click", event => staticRequest(event, StaticRequest.RESET));

	//init tabs
	for (const element of $(".pills > li")) {
		element.addEventListener("click", () => {
			const toggle = element.getAttribute("data-toggle");
			for (const item of $(".pills > li"))
				item.classList.remove("active");
			for (const tab of $(".tab"))
				tab.classList.remove("active");
			$(`#${toggle}`).classList.add("active");
			element.classList.add("active");
			Settings.actionMode = toggle;
		});
	}

	loadSettings();
	scan();
});