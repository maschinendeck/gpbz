#!/usr/bin/env python

"""
GPBZ: system_settings.py

This script runs as a pre-compilation task in platform.io.

It takes the members of the `SystemSettings` struct and creates three macros
inside the file `Settings_GENERATED.hpp`, which should provide some otherwise
tedious to write and to maintain code for JSON serializing, that is not possible
in c++ because of its limited preprocessor and lack of reflection.

Python is used here, because it is built-in in the platform.io build system. The
parsing is not very robust, but as we control the source, it is robust enough
for this special purpose.

-- MikO
"""

import re
import os
import errno

from dataclasses import dataclass

TypeInfoRegex = re.compile(r"([a-zA-Z0-9_\[\]:\(\)\"/']+|{.*})")

# terminal colors
class Color:
	GREEN : str = "\033[92m"
	RED   : str = "\033[91m"
	BLUE  : str = "\033[94m"

@dataclass
class OutputStruct:
	to_json : str = "#define SETTINGS_TO_JSON() \\\n"
	load    : str = "#define SETTINGS_LOAD() \\\n"
	set     : str = "#define SETTINGS_SET() \\\n"

# this parses and holds a typeinfo for a member of the `SystemSettings` struct
class Member:
	type       : str
	identifier : str
	default    : str
	flags      : list

	def __init__(self, line : str) -> None:
		typeInfo, *flags                          = line.split("//")
		self.type, self.identifier, *defaultValue = re.findall(TypeInfoRegex, typeInfo)
		self.default                              = defaultValue[0] if len(defaultValue) == 1 else None

		# removing the spaces in the flags part and split them on the '@'
		self.flags = set("".join(flags[0].split()).split("@")) if len(flags) == 1 else []
	
	def hasFlag(self, flag: str) -> bool:
		return flag in self.flags
	

def main() -> None:
	sourceHeader    = "include/util/SystemSettings.hpp"
	targetHeader    = "include/util/Settings_GENERATED.hpp"
	Output          = OutputStruct()
	needsGeneration = False

	# if target file does not exist, we need to generate anyway
	if not os.path.exists(targetHeader):
		needsGeneration = True
	# if source file does not exist, something went inherently wrong
	elif not os.path.exists(sourceHeader):
		print(f"{Color.RED}[system_settings.py] ERROR: missing `{sourceHeader}`. This is a crucial issue, your repository seems to be out of sync");
		exit(errno.ENOENT)
	# we only regenerate, if the source file has changed since last generation
	else:
		sourceTime      = os.path.getmtime(sourceHeader)
		targetTime      = os.path.getmtime(targetHeader)
		needsGeneration = sourceTime > targetTime

	if not needsGeneration:
		print(f"{Color.BLUE}[system_settings.py] generated header already exists. skipping.")
		return
	# open the `SystemSettings` header for reading and parsing
	with open(sourceHeader) as file:
		lines    = file.readlines()
		inStruct = False

		for line in lines:
			line = line.strip()

			# isolating the `SystemSettings` struct
			if line.startswith("struct SystemSettings"):
				inStruct = True
				continue
			# ignoring functions and empty lines; everything non-important
			if line.endswith("{") or line.startswith("return") or line == "}" or line == "":
				continue;
			if not inStruct:
				continue
			if line.startswith("};") and inStruct:
				inStruct = False
				continue

			# parsing type
			T = Member(line)

			# if a type has the '@ignore' flag, we omit it
			if T.hasFlag("ignore"):
				continue

			# handling the `SETTINGS_TO_JSON` and `SETTINGS_LOAD` macros
			Output.to_json += f"\tthis->jsonDoc[\"{T.identifier}\"] = this->settings->{T.identifier};" + "  \\\n"
			Output.load    += f"\tthis->settings->{T.identifier} = this->jsonDoc[\"{T.identifier}\"] | {T.default};  \\\n"

			# if a type has the '@readonly' flag, we omit it from the `SETTINGS_SET` macro
			if T.hasFlag("readonly"):
				continue

			# handling the `SETTINGS_SET` macro
			# the double `{{` is python escaping, so we effectively get one
			# single `{`
			Output.set += f"\telse if (key == \"{T.identifier}\") {{  \\\n"
			Output.set += f"\t\tthis->settings->{T.identifier} = std::move(maschinendeck::convert<{T.type}>(value));  \\\n"
			Output.set += "\t}  \\\n"

		# we open the output file for writing the generated code
		with open(targetHeader, "w") as file:
			file.write(f"{Output.to_json[:-2]}\n\n")
			file.write(f"{Output.load[:-2]}\n\n")
			file.write(f"{Output.set[:-2]}\n")

	print(f"{Color.GREEN}[system_settings.py] successfully generated `{targetHeader}`.")


main()