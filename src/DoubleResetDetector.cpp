#include <Esp.h>

#include "DoubleResetDetector.hpp"

namespace maschinendeck {

DoubleResetDetector::DoubleResetDetector() :
		detected_(false),
		currentlySetting(false),
		flag(DoubleResetDetector::Clear),
		timeout(millis() + DoubleResetDetector::TimeoutSeconds * 1000) {

		if (!this->detected()) {
			this->flag = DoubleResetDetector::Set;
			ESP.rtcUserMemoryWrite(DoubleResetDetector::Address, &this->flag, sizeof(this->flag));
			this->currentlySetting = true;
		} else
			this->detected_ = true;
}

bool DoubleResetDetector::detected() {
	if (this->currentlySetting)
		return false;
	if (this->detected_)
		return true;

	this->flag = DoubleResetDetector::Clear;
	ESP.rtcUserMemoryRead(DoubleResetDetector::Address, &this->flag, sizeof(this->flag));

	return this->flag == DoubleResetDetector::Set;
}

void DoubleResetDetector::clear() {
	this->flag = DoubleResetDetector::Clear;
	ESP.rtcUserMemoryWrite(DoubleResetDetector::Address, &this->flag, sizeof(this->flag));
}

void DoubleResetDetector::update() {
	if (millis() > this->timeout)
		this->clear();
}

}