#include <ArduinoOTA.h>
#include <Esp.h>
#include <LittleFS.h>
#include <SPI.h>

#include "ActionHandler.hpp"
#include "GPBZ.hpp"

namespace maschinendeck {

GPBZ::GPBZ() :
		settings_(*this),
		statusLight_(*this),
		wifi_(*this),
		webInterface(*this),
		lastTime(millis()) {

	#ifdef DEBUG
	Serial.begin(115200);
	Serial.println("");
	#endif
	//SPI.begin();

	bool fsReady = LittleFS.begin();
	if (fsReady) {
		this->logger_ << "[GPBZ] LittleFS ready" << std::endl;
	} else {
		this->logger_ << "[GPBZ] Initializing LittleFS..." << std::endl;
		LittleFS.format();
	}
	this->settings_.load();	
	this->statusLight_.set(StatusLight::COLOR_INFO);
	// 1. if ssid and password present, try client mode
	// 2. if not present or client mode fails -> ap mode
	if (!this->drd.detected() && this->settings_.wifiSettingsPresent()) {
		bool success = this->wifi_.clientMode(this->settings_->wifiSsid, this->settings_->wifiPassword, this->settings_->wifiChannel);
		if (success) {
			this->regularOperation();
			return;
		}
	}

	bool success = this->wifi_.apMode();

	if (!success) {
		this->statusLight_.set(StatusLight::COLOR_ERROR);
		this->logger_ << "[GPBZ] ERROR: FATAL: could not start `apMode`. Something is inherently wrong!" << std::endl;
		delay(1000);
		return;
	}
	// 3. start webserver
	this->webInterface.init();
	this->webInterface.listen();
}

void GPBZ::regularOperation() {
	ActionHandler actionHandler(*this);
	bool success = false;

	switch(this->settings_->actionMode) {
		case ActionMode::http:
			success = actionHandler.runWifi(this->settings_->targetURL);
			break;
		case ActionMode::mqtt:
			success = actionHandler.runMQTT(
				this->settings_->mqttServer,
				this->settings_->mqttPort,
				this->settings_->mqttUser,
				this->settings_->mqttPassword,
				this->settings_->mqttTopic,
				this->settings_->mqttMessage
			);
			break;
		case ActionMode::none:
		default:
			this->logger_ << "[GPBZ] ERROR: unknown action mode received." << std::endl;
			break;
	}
	this->drd.clear();

	if (success)
		this->statusLight_.set(StatusLight::COLOR_SUCCESS);
	else
		this->statusLight_.set(StatusLight::COLOR_ERROR);
	
	delay(1000);
	this->sleep();
}

void GPBZ::update() {
	uint32_t time      = millis();
	float    deltaTime = static_cast<float>(time - this->lastTime) / 1000.0f;

	this->drd.update();
	this->statusLight_.update(deltaTime);
	ArduinoOTA.handle();
	this->wifi_.update();
	this->webInterface.update();

	this->lastTime = time;
}

void GPBZ::sleep() {
	this->logger_ << "[GPBZ] going to sleep" << std::endl;
	this->statusLight_.shutdown();
	ESP.deepSleep(0);
}

void GPBZ::restart() {
	this->logger_ << "[GPBZ] restartings ESP" << std::endl;
	ESP.restart();
}

}