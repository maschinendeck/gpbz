#include <ArduinoJson.h>
#include <LittleFS.h>
#include <sstream>

#include "GPBZ.hpp"
#include "WebInterface.hpp"
#include "util/Logger.hpp"

namespace maschinendeck {

WebInterface::WebInterface(GPBZ& app) :
		app(app),
		log(this->app.logger()),
		socket(80),
		hadConnection(false),
		timeout(millis() + WebInterface::NoConnectionTimeout * 1000) {
}

void WebInterface::init() {
	this->app.statusLight().set(StatusLight::COLOR_WARNING, true);
	this->socket.addHook([this](
			const String& method,
			const String& url,
			WiFiClient* client,
			ESP8266WebServer::ContentTypeFunction contentType
		) -> ESP8266WebServer::ClientFuture {
		(void) method;
		(void) url;
		(void) client;
		(void) contentType;
		this->hadConnection = true;

		log << "[WebInterface] connection to '" << url.c_str() << "'" << std::endl;
  
		return ESP8266WebServer::CLIENT_REQUEST_CAN_CONTINUE;
	});

	this->socket.onNotFound([this]() -> void {
		fs::File fp = LittleFS.open("/index.html", "r");
		if (!fp)
			return;
		this->socket.send(200, "text/html", fp);
		fp.close();
	});

	this->socket.on("/scan", HTTP_GET, [this]() -> void {
		std::stringstream buffer;
		buffer << "[";
		for (const auto& network : this->app.wifi().scan())
			buffer << static_cast<std::string>(network) << ",";
		buffer.seekp(-1, buffer.cur); // remove last ','
		buffer << "]";

		this->socket.send(200, "application/json", buffer.str().c_str());
	});

	this->socket.on("/settings", HTTP_GET, [this]() -> void {
		this->socket.send(200, "application/json", this->app.settings().json().c_str());
	});

	this->socket.on("/settings", HTTP_POST, [this]() -> void {
		for (uint8_t i = 0; i < this->socket.args(); ++i) {
			std::string name = this->socket.argName(i).c_str();
			std::string value = this->socket.arg(name.c_str()).c_str();
			this->app.settings().set(name, value);
		}
		this->app.settings().save();

		log << "[WebInterface] received and saved settings from user. Restarting..." << std::endl;

		this->socket.send(200);

		this->app.restart();
	});

	this->socket.on("/clearCache", HTTP_GET, [this]() -> void {
		this->app.settings().forgetCachedWifiAttributes();
		this->socket.send(200);
	});

	this->socket.on("/resetSettings", HTTP_GET, [this]() -> void {
		this->app.settings().clear();
		this->socket.send(200);
	});

	this->socket.on("/restart", HTTP_GET, [this]() -> void {
		this->app.restart();
		this->socket.send(200);
	});

	this->socket.serveStatic("/", LittleFS, "/");
}

void WebInterface::listen() {
	this->socket.begin();
}

void WebInterface::update() {
	if (!this->hadConnection && (millis() > this->timeout)) {
		log << "[WebInterface] no connection for " << std::to_string(WebInterface::NoConnectionTimeout) << " seconds, going to sleep" << std::endl;
		this->app.sleep();
	}
	this->socket.handleClient();
}

}