#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>

#include "ActionHandler.hpp"
#include "GPBZ.hpp"

namespace maschinendeck {

ActionHandler::ActionHandler(GPBZ& app) : app(app), log(this->app.logger()) {
}

bool ActionHandler::runMQTT(
			const IPAddress& server,
			uint16_t port,
			const std::string& user,
			const std::string& password,
			const std::string& topic,
			const std::string& payload
		) const {
	 	WiFiClient client;
		PubSubClient mqtt(client);
		mqtt.setServer(server, port);
		uint8_t i = 0;
		while (!mqtt.connected()) {
			bool success;

			if (user.length() < 1 && password.length() < 1)
				success = mqtt.connect(ActionHandler::ClientID);
			else
				success = mqtt.connect(ActionHandler::ClientID, user.c_str(), password.c_str());

			if (success) {
				mqtt.publish(topic.c_str(), payload.c_str());
				break;
			} else {
				if (i >= ActionHandler::MQTTMaxRetries) {
					log << "[ActionHandler] ERROR: failed to speak to MQTT server:" << util::Logger::endl;
					log << "    Server   : " << server << util::Logger::endl; 
					log << "    Port     : " << std::to_string(port) << util::Logger::endl; 
					log << "    User     : '" << user << "'" << util::Logger::endl; 
					log << "    Password : '" << password << "'" << util::Logger::endl; 
					log << "    Topic    : " << topic << util::Logger::endl; 
					log << "    Payload  : '" << payload << "'" << std::endl; 
					return false;
				}
				delay(100);
				i++;
			}
		}
		mqtt.disconnect();
		log << "[ActionHandler] successfully sent payload '" << payload << "' on topic '" << topic << "' to server " << server.toString().c_str() << std::endl;

		return true;
}

bool ActionHandler::runWifi(const std::string& url) const {
	HTTPClient wget;

	bool secure = url.length() > 4 && url.at(4) == 's' ? true : false;

	if (!secure) {
		WiFiClient client;
		wget.begin(client, url.c_str());
	} else {
		WiFiClientSecure client;
		client.setInsecure();
		wget.begin(client, url.c_str());
	}
	int statusCode = wget.GET();
	bool success   = statusCode >= 200 && statusCode < 400;

	if (success)
		log << "[ActionHandler] successfully called '" << url << "' [" << statusCode << "]" << std::endl;
	else
		log << "[ActionHandler] ERROR calling '" << url << "' – got HTTP Code " << std::to_string(statusCode) << std::endl;

	return success;
}

}