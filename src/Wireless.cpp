#include "GPBZ.hpp"
#include "Wireless.hpp"
#include "util/Logger.hpp"

namespace maschinendeck {

const IPAddress Wireless::IP      = {192,168,4,1};
const IPAddress Wireless::Gateway = {192,168,4,0};
const IPAddress Wireless::Netmask = {255,255,255,0};

Wireless::Wireless(GPBZ& app) : app(app), log(this->app.logger()) {
}

bool Wireless::apMode() {
	log << "[Wireless] starting apMode..." << std::endl;
	WiFi.softAPConfig(Wireless::IP, Wireless::Gateway, Wireless::Netmask);
	WiFi.softAP("GPBZ");

	log << "[Wireless] spoofing DNS for captive portal..." << std::endl;
	this->dnsServer.disableForwarder();
	this->dnsServer.setErrorReplyCode(DNSReplyCode::NoError);
	this->dnsServer.start(Wireless::DNSPort, "*", Wireless::IP);

	return true;
}

bool Wireless::clientMode(std::string ssid, std::string password, int32_t channel) {
	util::Settings& settings = this->app.settings();

	bool hadInformation = false;

	WiFi.persistent(true);
	WiFi.mode(WIFI_STA);
	if (channel > 0 && settings->apMAC[0] != 0) {
		// this is the fast path. providing all this information should reduce
		// connection time drastically
		log << "[Wireless] had information saved. trying to connect to existing network '" << settings->wifiSsid << "'..." << std::endl;
		WiFi.config(
			settings->wifiIP,
			settings->wifiGateway,
			settings->wifiNetmask,
			settings->wifiDNS
		);
		WiFi.begin(ssid.c_str(), password.c_str(), channel, settings->apMAC, true);
		hadInformation = true;
	} else
		WiFi.begin(ssid.c_str(), password.c_str());

	uint32_t timeout = millis() + Wireless::ConnectionTimeout * 1000;
	while (WiFi.status() != WL_CONNECTED) {
		if (millis() % 1000 == 0)
			Serial.print('.');
		if (millis() >= timeout) {
			log << "[Wireless] Max retry count reached. give up connecting to " << this->app.settings()->wifiSsid << std::endl;
			if (hadInformation)
				settings.forgetCachedWifiAttributes();
			return false;
		}
		delay(10);
	}
	settings->wifiSsid    = WiFi.SSID().c_str();
	settings->wifiChannel = WiFi.channel();
	settings->wifiIP      = WiFi.localIP();
	settings->wifiNetmask = WiFi.subnetMask();
	settings->wifiGateway = WiFi.gatewayIP();
	settings->wifiDNS     = WiFi.dnsIP();
	uint8_t* mac = WiFi.BSSID();
	memcpy(settings->apMAC, mac, 6);

	log << "[Wireless] connected successfully." << util::Logger::endl;
	log << "    SSID       : " << settings->wifiSsid << util::Logger::endl;
	log << "    MAC        : " << settings.mac() << std::endl;
	log << "    Channel    : " << settings->wifiChannel << util::Logger::endl;
	log << "    IPv4       : " << settings->wifiIP << util::Logger::endl;
	log << "    Netmask    : " << settings->wifiNetmask << util::Logger::endl;
	log << "    Gateway    : " << settings->wifiGateway << util::Logger::endl;
	log << "    DNS Server : " << settings->wifiDNS << std::endl;

	if (!hadInformation)
		settings.save();

	return true;
}

std::vector<Network> Wireless::scan() const {
	log << "[Wireless] scanning for networks..." << std::endl;
	int8_t numberOfNetworks = WiFi.scanNetworks();
	std::vector<Network> networks;
	for (int8_t i = 0; i < numberOfNetworks; ++i)
		networks.emplace_back(i);
	log << "    found " << networks.size() << " networks." << std::endl;

	return networks;
}

void Wireless::update() {
	this->dnsServer.processNextRequest();
}

}