#include <Arduino.h>

#include "GPBZ.hpp"
#include "StatusLight.hpp"
#include "util/Logger.hpp"
#include <SPI.h>

namespace maschinendeck {

StatusLight::StatusLight(GPBZ& app) :
		log(app.logger()),
		pixels(StatusLight::WS2812_COUNT, StatusLight::WS2812_PIN, NEO_GRB + NEO_KHZ800),
		pulse(false),
		intensity(0),
		direction(Direction::UP),
		color({0, 0, 0}) {
	pinMode(StatusLight::BOOST_CONVERTER_PIN, OUTPUT);
	digitalWrite(StatusLight::BOOST_CONVERTER_PIN, HIGH);
	this->pixels.begin();
}

void StatusLight::set(Color color, bool pulse) {
	this->color = color;
	this->pulse = pulse;
	this->pixels.begin();
	this->pixels.clear();
	this->pixels.fill(Adafruit_NeoPixel::Color(color.r, color.g, color.b));
	this->pixels.show();
}

void StatusLight::update(float deltaTime) {
	uint32_t time      = millis();

	if (this->pulse) {
		float intensity = this->intensity;
		this->pixels.fill(this->color);

		switch (this->direction) {
			case Direction::UP:
				intensity += StatusLight::BrightnessMultiplier * deltaTime;
				if (intensity >= 255)
					this->direction = Direction::DOWN;
				break;
			case Direction::DOWN:
				intensity -= StatusLight::BrightnessMultiplier * deltaTime;
				if (intensity <= 0)
					this->direction = Direction::UP;
				break;
		}
		this->intensity = intensity >= 255 ? 255 : (intensity <= 0 ? 0 : intensity);
		this->pixels.setBrightness(static_cast<uint8_t>(this->intensity));
		this->pixels.show();
	}
}

void StatusLight::shutdown() const {
	digitalWrite(StatusLight::BOOST_CONVERTER_PIN, LOW);
}

}