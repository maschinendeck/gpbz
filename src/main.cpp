#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <memory>

#include "GPBZ.hpp"

std::unique_ptr<maschinendeck::GPBZ> app;

void setup() {
	app = std::make_unique<maschinendeck::GPBZ>();
}

void loop() {
	app->update();
}