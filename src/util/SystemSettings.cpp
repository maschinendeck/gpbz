#include <assert.h>

#include "util/SystemSettings.hpp"

namespace maschinendeck {

std::string ActionModeToJSON(ActionMode mode) {
	int index = static_cast<int>(mode);
	return std::string(ActionModeJSON[index]);
}

ActionMode ActionModeFromJSON(const std::string& json) {
	uint8_t index = 2;
	for (size_t i = 0; i < std::size(ActionModeJSON); ++i) {
		if (json == ActionModeJSON[i])
			index = i;
	}
	
	return static_cast<ActionMode>(index);
}

}