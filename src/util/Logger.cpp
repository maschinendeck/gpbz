#include <iomanip>

#include "util/Logger.hpp"

namespace maschinendeck::util {

template <>
Logger& Logger::operator <<(const IPAddress& text) {
	#ifdef DEBUG
	this->currentLine << text.toString().c_str();
	#else
	(void) text;
	#endif
	return *this;
}

template <>
Logger& Logger::operator <<(uint8_t* const& mac) {
	#ifdef DEBUG
	for (uint8_t i = 0; i < 6; ++i) {
		this->currentLine << std::hex << std::setw(2) << static_cast<int>(mac[i]);
		if (i < 5)
			this->currentLine << ":";
	}
	#else
	(void) mac;
	#endif
	return *this;
}

}