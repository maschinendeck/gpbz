#include <ArduinoJson.h>
#include <LittleFS.h>
#include <sstream>

#include "util/Files.hpp"
#include "GPBZ.hpp"
#include "util/Settings.hpp"
#include "util/Settings_GENERATED.hpp"

namespace maschinendeck::util {

Settings::Settings(maschinendeck::GPBZ& app) :
		app(app),
		log(app.logger()),
		settings(std::make_shared<SystemSettings>()) {
}

void Settings::toJSON() {
	// this uses an auto generated macro, generated by `system_settings.py`
	SETTINGS_TO_JSON();

	JsonArray mac = this->jsonDoc["apMAC"].to<JsonArray>();
	for (uint8_t i = 0; i < 6; ++i)
		mac.add(this->settings->apMAC[i]);
}

const std::string& Settings::json() {
	this->json_.clear();
	serializeJson(this->jsonDoc, this->json_);

	return this->json_;
}

void Settings::init() {
	fs::File fp = LittleFS.open(Settings::Filename, "w");

	if (!fp) {
		log << "[Settings] could not open settings.json for writing" << std::endl;
		return;
	}
	fp.write("{}");
	fp.close();
}

void Settings::load() {
	if (!LittleFS.exists(Settings::Filename))
		this->init();

	fs::File fp = LittleFS.open(Settings::Filename, "r+");

	if (!fp) {
		log << "[Settings] could not open settings.json for reading" << std::endl;
		return;
	}

	DeserializationError error = deserializeJson(this->jsonDoc, fp);

	#ifdef DEBUG
	if (error) {
		log << "[Settings] failed to deserialize settings.json. restarting." << util::Logger::endl;
		log << "    " << error.c_str() << util::Logger::endl;
		
		log << fileContents(fp).str() << std::endl;

		LittleFS.remove(Settings::Filename);
		fp.print("{}");
		fp.close();
		this->app.restart();
	}
	#endif

	// this uses an auto generated macro, generated by `system_settings.py`
	SETTINGS_LOAD();

	uint8_t i = 0;
	for (const auto& part : this->jsonDoc["apMAC"].as<JsonArray>()) {
		this->settings->apMAC[i] = part.as<uint8_t>();
		i++;
	}

	this->toJSON();
	log << "[Settings] settings have been loaded successfully." << std::endl;

	fp.close();
}

void Settings::save() {
	fs::File fp = LittleFS.open(Settings::Filename, "w");

	if (!fp) {
		log << "[Settings] could not open settings.json for writing on save" << std::endl;
		return;
	}

	this->toJSON();
	serializeJson(this->jsonDoc, fp);
	log << "[Settings] saved!" << std::endl;

	fp.close();
}

void Settings::set(const std::string& key, const std::string& value) {
	if (key == "wifiSsid") {
		if (this->settings->wifiSsid != value)
			this->forgetWifi();
		this->settings->wifiSsid = value;
	}
	// this uses an auto generated macro, generated by `system_settings.py`
	SETTINGS_SET()
	else
		return;
}

void Settings::clear() {
	LittleFS.remove(Settings::Filename);
	fs::File fp = LittleFS.open(Settings::Filename, "w");
	fp.print("{}");
	fp.close();
	this->load();
}

}

namespace maschinendeck {
	void convertFromJson(JsonVariantConst& src, ActionMode& dst) {
		dst = ActionModeFromJSON(src.as<std::string>());
	}

	bool convertToJson(const ActionMode& src, JsonVariant dst) {
		return dst.set(std::move(ActionModeToJSON(src)));
	}

	bool canConvertFromJson(JsonVariantConst src, const ActionMode& dst) {
		return src.is<JsonString>();
	}
}

void convertFromJson(JsonVariantConst& src, IPAddress& dst) {
	IPAddress address;
	address.fromString(src.as<JsonString>().c_str());
	dst = address;
}

bool convertToJson(const IPAddress& src, JsonVariant dst) {
	return dst.set(src.toString());
}

bool canConvertFromJson(JsonVariantConst src, const IPAddress& dst) {
	return src.is<JsonString>();
}